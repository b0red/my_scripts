

#!/usr/bin/env bash
# 
# Small script for backing up my_scripts foldercontent

# Source files
source variables.sh
source email_variables.sh

# Variables
DIR=/tmp
FILE=Backup

# Check if zip exists
#  it's possible to add more commands to check for, in the brackets
CMDS="zip msmtp droptobox"

for i in $CMDS
do
	type -P $i &>/dev/null  && continue  || { echo "$i command not found. Please install!"; exit 1; }
done

# Create zipfile, omit .git-stuff
if zip -r $DIR/$FILE-$DATE.zip /my_scripts/* -x .git .gitignore
then
	droptobox upload $DIR/$FILE-$DATE.zip $STORAGEPATH
	rm $DIR/$FILE-$DATE.zip 
	echo "Klar. Zippat och laddat upp $FILE till $STORAGEPATH till dropbox, maskin: $MACHINE $IP" |\
	msmtp -a gmail $EMAIL_P
	echo "Klar! @$IP"
else
	echo "Failed! No backup done!"
fi

