#!/usr/bin/env bash
#
# Variables inc file

# Get dates
DATE=$(date +%Y%m%d-%H%M%S)
SHORTDATE=$(date +%Y%m%d)

# Get or set local Variables
MACHINE=$(uname -n)
OS=`uname`
IO=""
OPS=`uname -o`
RELEASE=`uname -r`
PLACE=`uname -n`

# Set Dropboxvariables
DB_DEST='/Machines'
MACHINE=$OPS
WORK='/Work'
BACKUP='/Backups'
Common='/Common'

# Make the storagepath in Dropbox
STORAGEPATH=${DB_DEST}/${OS}/${OPS}_${RELEASE}

# Get and store local IP
case $OS in
	Linux) IP=`ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}'`;;
	FreeBSD|OpenBSD) IP=`ifconfig  | grep -E 'inet.[0-9]' | grep -v '127.0.0.1' | awk '{ print $2}'` ;;
	SunOS|Solaris) IP=`ifconfig -a | grep inet | grep -v '127.0.0.1' | awk '{ print $2} '` ;;
	*) IP="Unknown";;
esac
export LocalIP=$IP;#echo vari: $LocalIP

if test "$1" = "P"
  then
    clear;echo $'\n'
    echo "Date:		$DATE"
    echo "Shortdate: 	$SHORTDATE"$'\n'
    echo "Machine: 		$MACHINE"
    echo "OS: 			$OS"
    echo "OPS: 			$OPS"
    echo "Release: 		$RELEASE"
    echo "Place: 			$PLACE"
    echo "DestinatioN: 		$DB_DEST"
    echo "Machine: 		$MACHINE"
    echo "OPS: 			$OPS"
    echo "IP:			$IP"
    echo "LocalIP:		$LocalIP"
    echo "StoragePath: 		$STORAGEPATH"$'\n'
fi
