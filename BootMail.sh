#!/usr/bin/env bash
# Shell script scripts to read ip address
# -------------------------------------------------------------------------
# Copyright (c) 2005 nixCraft project <http://cyberciti.biz/fb/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of nixCraft shell script collection (NSSC)
# Visit http://bash.cyberciti.biz/ for more information.
# -------------------------------------------------------------------------
source email_variables.sh

# Get OS name
OS=`uname`
IO=""

#store IP
case $OS in
   Linux) IP=`ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}'`;;
   FreeBSD|OpenBSD) IP=`ifconfig  | grep -E 'inet.[0-9]' | grep -v '127.0.0.1' | awk '{ print $2}'` ;;
   SunOS) IP=`ifconfig -a | grep inet | grep -v '127.0.0.1' | awk '{ print $2} '` ;;
   *) IP="Unknown";;
esac

#EMAIL="root"
#STARTSUBJ=`hostname`" started on "`date`
#STARTBODY="Just letting you know that server "`hostname`" has started on "`date`
#STOPSUBJ=`hostname`" shutdown on "`date`
#STOPBODY="Just letting you know that server "`hostname`" has shutdown on "`date`

# remmar lockfile
#lockf=/var/lock/subsys/emailstartstop

# Send email on startup
start() {
   # echo "Sending email on startup: "
    echo "Server startad @$IP" | msmtp -a gmail $EMAIL2    
    #echo "${STARTBODY}" | mail -s "${STARTSUBJ}" ${EMAIL1}
    #RETVAL=$?
    #echo
    #[ $RETVAL = 0 ]## && touch $lockf
    #return 0
}

# Send email on shutdown
stop() {
    echo -n "Sending email on shutdown: "

    echo "Stop @$IP" | msmtp -a gmail $EMAIL2
    #echo "${STOPBODY}" | mail -s "${STOPSUBJ}" ${EMAIL}
   # RETVAL=$?
   # echo
   # [ $RETVAL = 0 ] && ## rm -f $lockfile
   # return 0
}

# See how we were called.
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    *)
        echo $"Usage: $prog {start|stop}"
        exit 2
esac
exit ${RETVAL}


