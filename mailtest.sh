source /my_scripts/emailvariables.sh
ip=`echo $SSH_CONNECTION | cut -d " " -f 1`
box=`uname -or`
logger -t ssh-wrapper $USER login from $ip
DATE=$(date +%h-%d-%Y%n%H:%M:%S)
echo "To: $EMAIL2" > /tmp/login.mail
echo "From $EMAIL1+$box@gmail.com" >> /tmp/login.mail
echo "Subject: User $USER logged in from $ip $box" >> /tmp/login.mail
echo "User $USER logged in from $ip $box $DATE" >> /tmp/login.mail
more /tmp/login.mail | msmtp -a gmail $EMAIL2
rm /tmp/login.mail
