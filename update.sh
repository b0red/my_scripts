#!/bin/sh

# This is an update script written for a FreeBSD 9.2 system using PC-BSD pkgng repo
# This script was placed in /root/bin/
# Author: Phil Baker  https://plus.google.com/+PhilBaker1


# Documentation ----------------------------------------------------------

# FreeBSD Update:
# http://www.freebsd.org/doc/handbook/updating-upgrading-freebsdupdate.html

# pkgng:
# http://www.freebsd.org/doc/handbook/pkgng-intro.html

# Switching to the PC-BSD pkgng Repository (PC-BSD Wiki):
# http://goo.gl/Szzjvz

# Sample /usr/local/etc/pkg.conf (remove one leading '#' per line before using):
   ## packagesite: http://pkg.FreeBSD.org/freebsd:9:x86:64/latest

   ## Added by Phil
   # packagesite: http://pkg.cdn.pcbsd.org/9.2-RELEASE/amd64
   # PUBKEY: /usr/local/etc/pkg-pubkey.cert
   # PKG_CACHEDIR: /usr/local/tmp

   ## End of /usr/local/etc/pkg.conf

# Ports:
# http://www.freebsd.org/doc/handbook/ports-using.html

# End Documentation -------------------------------------------------------


echo ">>> Running FreeBSD Update on base system...";
freebsd-update fetch;
freebsd-update install;

echo " ";
echo ">>> Updating package list...";
pkg update;

echo " ";
echo ">>> Upgrading packages...";
pkg upgrade;

# Sometimes the 'pkg' package needs updating first, followed by 
# an update of the remaining packages.  So we repeat...
echo " ";
echo ">>> Upgrading packages (second pass)..."
pkg upgrade;

echo " ";
echo ">>> Remove stale dependencies...";
pkg autoremove;

echo " ";
echo ">>> Clean local package cache...";
pkg clean;

echo " ";
echo ">>> Audit installed packages for known security vulnerabilities...";
pkg audit -F > /root/tmp/pkgaudit.txt;
cat /root/tmp/pkgaudit.txt;

# Upgrade Ports
echo " ";
echo ">>> Ports...";
read -p "Do you wish to update the Ports tree? " yn
case $yn in
    [Yy]* ) portsnap fetch; 
            portsnap update;
            echo " ";
            echo "Relisting package vulnerabilities...";
	        cat /root/tmp/pkgaudit.txt;;
    [Nn]* ) echo "Ports tree not updated.";;
    * ) echo "Please answer yes or no.";;
esac

# Equivalent of 'rehash' in csh:
hash -r;

echo " ";
echo ">>> Finished. Grab a beer.";

exit

