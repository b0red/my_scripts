#!/bin/sh
# Shell script scripts to read ip address
# -------------------------------------------------------------------------
# Copyright (c) 2005 nixCraft project <http://cyberciti.biz/fb/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of nixCraft shell script collection (NSSC)
# Visit http://bash.cyberciti.biz/ for more information.
# -------------------------------------------------------------------------
source email_variables.sh

# Get OS name
OS=`uname`
IO=""
Tid=$(date +%T)
#store IP
case $OS in
   Linux) IP=`ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}'`;;
   FreeBSD|OpenBSD) IP=`ifconfig  | grep -E 'inet.[0-9]' | grep -v '127.0.0.1' | awk '{ print $2}'` ;;
   SunOS) IP=`ifconfig -a | grep inet | grep -v '127.0.0.1' | awk '{ print $2} '` ;;
   *) IP="Unknown";;
esac

case $1 in
start)
	echo "$IP startad @$Tid" | msmtp -a gmail EMAIL2
	echo "Server startad @$IP/$Tid"
;;
stop)
	echo "$IP stoppad @$Tid" | msmtp -a gmail EMAIL2
	echo "Server stoppad @$IP/$Tid"
;;
esac
exit 
