#!/usr/bin/env bash

/usr/sbin/portsnap fetch update && \
/usr/local/sbin/portmaster -L --index-only | egrep '(ew|ort) version|total install'
pkg updating -d `ls -ltr -D '%Y%m%d' /var/db/pkg | awk 'END{ print $6 }'` | less

